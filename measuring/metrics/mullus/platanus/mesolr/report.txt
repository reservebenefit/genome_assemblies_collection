All statistics are based on contigs of size >= 4000 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    mullus_gapclose_hpc_gapClosed
# contigs (>= 0 bp)         272725                       
# contigs (>= 1000 bp)      27638                        
# contigs (>= 5000 bp)      2940                         
# contigs (>= 10000 bp)     2159                         
# contigs (>= 25000 bp)     1771                         
# contigs (>= 50000 bp)     1517                         
Total length (>= 0 bp)      630495908                    
Total length (>= 1000 bp)   577710634                    
Total length (>= 5000 bp)   534778755                    
Total length (>= 10000 bp)  529438335                    
Total length (>= 25000 bp)  523327264                    
Total length (>= 50000 bp)  514184196                    
# contigs                   3425                         
Largest contig              3278074                      
Total length                536930553                    
GC (%)                      45.14                        
N50                         479354                       
N75                         257789                       
L50                         317                          
L75                         698                          
# N's per 100 kbp           6706.19                      
