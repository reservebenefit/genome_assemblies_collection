All statistics are based on contigs of size >= 4000 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    serran_mbb_gapClosed
# contigs (>= 0 bp)         177982              
# contigs (>= 1000 bp)      20164               
# contigs (>= 5000 bp)      2190                
# contigs (>= 10000 bp)     1713                
# contigs (>= 25000 bp)     1485                
# contigs (>= 50000 bp)     1355                
Total length (>= 0 bp)      697914092           
Total length (>= 1000 bp)   656076523           
Total length (>= 5000 bp)   626940096           
Total length (>= 10000 bp)  623752971           
Total length (>= 25000 bp)  620167381           
Total length (>= 50000 bp)  615500364           
# contigs                   2469                
Largest contig              2764103             
Total length                628172492           
GC (%)                      40.32               
N50                         613332              
N75                         397188              
L50                         352                 
L75                         663                 
# N's per 100 kbp           3770.02             
