All statistics are based on contigs of size >= 4000 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    improved_serran
# contigs (>= 0 bp)         3102           
# contigs (>= 1000 bp)      3102           
# contigs (>= 5000 bp)      2122           
# contigs (>= 10000 bp)     1645           
# contigs (>= 25000 bp)     1418           
# contigs (>= 50000 bp)     1294           
Total length (>= 0 bp)      630579612      
Total length (>= 1000 bp)   630579612      
Total length (>= 5000 bp)   626940776      
Total length (>= 10000 bp)  623753651      
Total length (>= 25000 bp)  620187813      
Total length (>= 50000 bp)  615743990      
# contigs                   2401           
Largest contig              2764103        
Total length                628173172      
GC (%)                      40.32          
N50                         629123         
N75                         421491         
L50                         342            
L75                         643            
# N's per 100 kbp           3770.13        
