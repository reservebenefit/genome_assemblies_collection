from Bio import SeqIO
import sys
import numpy

input = open(sys.argv[1], 'rU')
long_sequences = [] # Setup an empty list
for record in SeqIO.parse(input, "fasta"):
    if len(record.seq) > 3000:
        # Add this record to our list
        long_sequences.append(record)
SeqIO.write(long_sequences, "long_seqs_3000.fasta", "fasta")

