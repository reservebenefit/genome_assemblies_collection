## dependencies
sudo apt-get update && sudo apt-get install -y pkg-config libfreetype6-dev libpng-dev python-matplotlib

## Quast
git clone https://github.com/ablab/quast.git
cd quast
./setup.py install
