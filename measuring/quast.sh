###############################################################################
## serranus cabrilla

## metrics serran supernova assembly first try
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/serran/supernova_try1 \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_serran/mbb/chromium/try_1/serranus.1.fasta \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_serran/mbb/chromium/try_1/serranus.2.fasta

## metrics serran supernova assembly second try
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/serran/supernova_try2 \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_serran/mbb/chromium/try_2/serranus2.1.fasta \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_serran/mbb/chromium/try_2/serranus2.2.fasta


## metrics serran platanus assembly
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/serran/platanus \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_serran/mbb/platanus/assemblies/serran_mbb_gapClosed.fa


## metrics serran ARCS assembly
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/serran/arcs \
/home/pguerin/working/draft_genome_assembly/serran_arcs/improved_serran.fasta


###############################################################################
## mullus surmuletus

## metrics mullus platanus [hpc:MBB] assembly
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/mullus/platanus/mbb \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_mullus/mbb/gapclose_platanus_mbb/mullus_gapclose_mbb_gapClosed.fa

## metrics mullus platanus [hpc:MESO@LR] assembly
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/mullus/platanus/mesolr \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_mullus/hpc/gapclose_platanus_hpc/mullus_gapclose_hpc_gapClosed.fa


###############################################################################
## diplodus sargus

## metrics diplodus platanus [hpc:MBB] assembly
#

## metrics diplodus platanus [hpc:MESO@LR] assembly
quast.py --min-contig 4000 --eukaryote --circos --output-dir metrics/diplodus/platanus/mesolr \
/donnees/RESERVEBENEFIT/whole_genome_assembly/assemblage_sar/gapclose_platanus_hpc/sar_hpc_g_gapClosed.fa


###############################################################################
## cat metrics

paste <(awk -F"\t" '{ print $1}'  metrics/serran/supernova/report.tsv) \
<(awk -F"\t" '{ print $2}'  metrics/serran/supernova/report.tsv) \
<(awk -F"\t" '{ print $2}'  metrics/serran/platanus/report.tsv) \
<(awk -F"\t" '{ print $2}'  metrics/serran/arcs/report.tsv) \
<(awk -F"\t" '{ print $2}'  metrics/mullus/platanus/mbb/report.tsv) \
<(awk -F"\t" '{ print $2}'  metrics/mullus/platanus/mesolr/report.tsv) \
<(awk -F"\t" '{ print $2}' metrics/diplodus/platanus/mesolr/report.tsv) > metrics/all_report.tsv

