## first try
nohup ./programmes/supernova-2.1.1/supernova run --id=serran --fastqs=~/rawdata/ --localmem=470 --maxreads=298666666 &
nohup /media/bigvol/peguerin/programmes/supernova-2.1.1/supernova mkoutput --style=pseudohap2 --asmdir=/media/bigvol/peguerin/serran/outs/assembly --outprefix=serranus &

## second try
nohup ./supernova-2.1.1/supernova run --id=serran --fastqs=x_dir/ --localmem=470 --maxreads=250000000 &
nohup ./supernova-2.1.1/supernova mkoutput --style=pseudohap2 --asmdir=/media/bigvol/peguerin/serran/outs/assembly --outprefix=serranus2 &