sudo apt-get -y --force-yes update
sudo apt-get install --reinstall zlibc zlib1g zlib1g-dev
sudo apt-get install autotools-dev
yes | sudo apt-get install automake
sudo apt install wget
yes | sudo apt-get install libboost-all-dev

## install supernova
wget -O supernova-2.1.1.tar.gz "http://cf.10xgenomics.com/releases/assembly/supernova-2.1.1.tar.gz?Expires=1562810665&Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cDovL2NmLjEweGdlbm9taWNzLmNvbS9yZWxlYXNlcy9hc3NlbWJseS9zdXBlcm5vdmEtMi4xLjEudGFyLmd6IiwiQ29uZGl0aW9uIjp7IkRhdGVMZXNzVGhhbiI6eyJBV1M6RXBvY2hUaW1lIjoxNTYyODEwNjY1fX19XX0_&Signature=nfpZx59YO8QqmcOUt~pLdYOnNdfQ3zV-fOFHIUJm1DEUEH8aGI7L2wJo-qcianRe~QrdCBHUumI9kTrM8AjY-4Hi~pZfHMOZotSCsl1MzGjYoIEyjRM1RekkFDBeDldpC6tEL-Q9w-c8oWGVJL-MfI6rX56bluh~CIT534JYVhmkON8h3lMinzeqEyCW6BTbFeMa4OqzbZME1XOFUDq-D9-lPj3C8BVN2HB53V~LOtIJjZjHivyGW7bIdrfH0mQ94X0qmPebZAzkhYiLocrjnQyc9R0sm62r2btsOlErvXBqSRGD5PxE2lLJGtIuG8K5L6jOtn6fANwR3BLfoJz~~A__&Key-Pair-Id=APKAI7S6A5RYOXBWRPDA"
tar -xzvf supernova-2.1.1.tar.gz
echo "export PATH=$PATH:/media/bigvol/peguerin/supernova-2.1.1/" >> ~/.bashrc
