###define program path
export PATH=/media/bigvol/peguerin/src/longranger-2.2.2:$PATH
export PATH=/media/bigvol/peguerin/src/Platanus_allee_v2.0.1_Linux_x86_64:$PATH

pe1_R1=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-1_R1.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq
pe1_R2=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-1_R2.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq
pe2_R1=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-2_R1.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq
pe2_R2=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-2_R2.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq

mp1_R1=/media/bigvol/peguerin/180806_NB501850_A_L1-4_ANIZ-3_R1.fastq
mp1_R2=/media/bigvol/peguerin/180806_NB501850_A_L1-4_ANIZ-3_R2.fastq
mp2_R1=/media/bigvol/peguerin/180807_NB501850_A_L1-4_ANIZ-4_R1.fastq
mp2_R2=/media/bigvol/peguerin/180807_NB501850_A_L1-4_ANIZ-4_R2.fastq


Xbarcode=/media/bigvol/peguerin/xxx/outs/barcoded.fastq

platanus_allee phase \
-c out_contig.fa out_junctionKmer.fa \
-t 64 \
-IP1 $pe1_R1 $pe1_R2 \
-IP2 $pe2_R1 $pe2_R2 \
-OP3 $mp1_R1 $mp1_R2 \
-OP4 $mp2_R1 $mp2_R2 \
-x $Xbarcode \
2>phase.log