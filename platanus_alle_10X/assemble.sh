### download longranger
#wget -O longranger-2.2.2.tar.gz "http://cf.10xgenomics.com/releases/genome/longranger-2.2.2.tar.gz?Expires=1547519178&Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cDovL2NmLjEweGdlbm9taWNzLmNvbS9yZWxlYXNlcy9nZW5vbWUvbG9uZ3Jhbmdlci0yLjIuMi50YXIuZ3oiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE1NDc1MTkxNzh9fX1dfQ__&Signature=Hc44pebGAA9E6Aguhum0gHiuAnko-bxW-gftlHdNqt9mHKEjaIPxHaZLtym8QS4DaYic~LEBbc3p2uyQ7J-cnHhGE5hQDAihJzx3ur2UsTzZmESaEwRo9aTuwrHz34amN0MV~4I-X5-862-cfkZZfqy-c3N0tgB8lwRc0v1IZc1ULwVoNGLiMosxNgwMLlDzxU69gpugpIKhd-KCJ-v-UIl2inBVrDTTpv9cmj2jY7fabmrCXMmO-Itbi5w4OEGPS7N~KSMBqM8JQ-fHMhMDUNLSWSopMgT-sW6HPRPvWviFyXLMdN~wg8HcQsQTz7FNLn0AhY6YweOvtZtB7TK5ag__&Key-Pair-Id=APKAI7S6A5RYOXBWRPDA"

###define program path
export PATH=/media/bigvol/peguerin/src/longranger-2.2.2:$PATH
export PATH=/media/bigvol/peguerin/src/Platanus_allee_v2.0.1_Linux_x86_64:$PATH

pe1_R1=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-1_R1.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq.gz
pe1_R2=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-1_R2.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq.gz
pe2_R1=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-2_R1.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq.gz
pe2_R2=/media/bigvol/peguerin/180802_NB501473_A_L1-4_ANIZ-2_R2.RD30.NotEmpty.LinkerTrimmed-50bp-PR.fastq.gz

platanus_allee assemble -m 256 -t 64 -f $pe1_R1 $pe1_R2 $pe2_R1 $pe2_R2 2>assemble.log
