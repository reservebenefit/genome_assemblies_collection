sudo apt-get -y --force-yes update
sudo apt-get install --reinstall zlibc zlib1g zlib1g-dev
sudo apt-get install autotools-dev
yes | sudo apt-get install automake
sudo apt install wget
yes | sudo apt-get install libboost-all-dev

wget http://www.bcgsc.ca/platform/bioinfo/software/links/releases/1.8.6/links_v1-8-6.tar.gz
gunzip links_v1-8-6.tar.gz
tar -xvf links_v1-8-6.tar


sudo echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
sudo echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
sudo locale-gen

git clone https://github.com/bcgsc/arcs.git
cd arcs
./autogen.sh
./configure
make
sudo make install
cd ..
echo "export PATH=$PATH:/media/bigvol/peguerin/links_v1.8.6/" >> ~/.bashrc

## install longranger
wget -O longranger-2.2.2.tar.gz "http://cf.10xgenomics.com/releases/genome/longranger-2.2.2.tar.gz?Expires=1554242254&Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cDovL2NmLjEweGdlbm9taWNzLmNvbS9yZWxlYXNlcy9nZW5vbWUvbG9uZ3Jhbmdlci0yLjIuMi50YXIuZ3oiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE1NTQyNDIyNTR9fX1dfQ__&Signature=CWpdq48GhG9gCSvSYHSJcJWUMSBuw7h58tsdyDB~FqIf5ktXVJr4kN2t3ycjz9aTCtJ961d3dVyGMgiGxATtqO-ajEa0zNEihxf1nWMPxHMkLEZDo7-OO28MaJoXFsfo8a91PVVBL7UraVdHZ8ijdUG75wkoONHIo~VBX-OrdDY3RwtBLAc1AwB3sfOClw2~u5z-rGW8YeC49Vim8WHwzyK1Xp6ybAuqV9~mOEChLHNtYqmqIihmSFPoFWXtO7Lm~YCD~j-zg16xtiPWBQrvrlXuxd2V185MtraMrsxAm5Zd7FoypgjuNsFU3qo1PqznNbrPao8OOXZXZV8gcU10gw__&Key-Pair-Id=APKAI7S6A5RYOXBWRPDA"
tar -xzvf longranger-2.2.2.tar.gz
longranger testrun --id=tiny

echo "export PATH=$PATH:/media/bigvol/peguerin/longranger-2.2.2/" >> ~/.bashrc


## install Tigmint 
sudo apt-get install bedtools bwa samtools
sudo apt-get install seqtk abyss
sudo apt install python3-pip
pip3 install intervaltree pybedtools pysam statistics
git clone https://github.com/bcgsc/tigmint
echo "export PATH=$PATH:/media/bigvol/peguerin/tigmint/bin" >> ~/.bashrc

## install pigz
sudo apt install pigz

brew tap brewsci/bio


wget http://www.bcgsc.ca/downloads/supplementary/ARCS/testdata/links_c5r0.05e30000-l5-a0.9.scaffolds.fa


