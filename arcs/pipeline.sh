
## produce interleaved filefastq from 10X raw data
longranger basic --fastqs=/media/bigvol/peguerin/donnees/lib10x/ --id=xxx --sample=Lib_10
## Make CHROMIUM_interleaved.fastq file from Longranger out put (fastq file)
SERRAN_CHROMIUM_LONGRANGER="/media/bigvol/peguerin/xxx/outs/barcoded.fastq.gz"
SERRAN_CHROMIUM_INTERLAVED="/media/bigvol/peguerin/donnees/interleaved.fastq"
unpigz -c $SERRAN_CHROMIUM_LONGRANGER | perl -ne 'chomp;$ct++;$ct=1 if($ct>4);if($ct==1){if(/(\@\S+)\sBX\:Z\:(\S{16})/){$flag=1;$head=$1."_".$2;print "$head\n";}else{$flag=0;}}else{print "$_\n" if($flag);}' > $SERRAN_CHROMIUM_INTERLAVED


## set pipeline
cd ~/arcs/Examples/serran
mkdir ../serran
cp *sh ../serran
cp *py ../serran/
cd ../serran/


## Renumber your draft genome
SERRAN_DRAFT="/media/bigvol/peguerin/donnees/serran_genome_lgt3000.fasta"
SERRAN_ASSEMBLY="/media/bigvol/peguerin/donnees/serran_genome_renamed.fasta"
cat $SERRAN_DRAFT | perl -ne 'chomp;if(/^>/){$ct++;print ">$ct\n";}else{print "$_\n";}' > $SERRAN_ASSEMBLY

## index genome assembly
bwa index $SERRAN_ASSEMBLY
mv nohup.out index.log
## run alignments
bwa mem -t 48 $SERRAN_ASSEMBLY -p $SERRAN_CHROMIUM_INTERLAVED > aln_serran.sam
mv nohup.out aln.log
samtools view -Sb aln_serran.sam > aln_serran.bam
samtools sort -n aln_serran.bam -@ 48 -O BAM -o aln_serran.sorted.bam


#samtools view -Sb aln_serran.sam | samtools sort -n -o aln_serran.sorted.bam - &

## Create alignment.fof file and add full path of bam file 
echo "aln_serran.sorted.bam" > alignments.fof


## Running scaffolding method ARCS...
bash ARCS.sh 3 0.05 30000 $SERRAN_ASSEMBLY
mv nohup.out arcs.log

## Converting graph for LINKS...
ln -s /media/bigvol/peguerin/donnees/serran_genome_lgt3000.fasta.scaff_s98_c5_l0_d0_e30000_r0.05_original.gv serran_genome_lgt3000.fasta.scaff_s98_c5_l0_d0_e30000_r0.05_original.gv

SERRAN_GV="/media/bigvol/peguerin/donnees/serran_genome_renamed.fasta.scaff_s98_c4_l0_d0_e30000_r0.05_original.gv"
SERRAN_TSV="links_c4r0.05e30000-l5-a0.9.tigpair_checkpoint.tsv"
python makeTSVfile.py $SERRAN_GV $SERRAN_TSV $SERRAN_ASSEMBLY

ln -s /media/bigvol/peguerin/donnees/serran_genome_renamed.fasta.scaff_s98_c4_l0_d0_e30000_r0.05_original.gv serran_genome_renamed.fasta.scaff_s98_c4_l0_d0_e30000_r0.05_original.gv
ln -s /media/bigvol/peguerin/donnees/serran_genome_renamed.fasta.scaff_s98_c4_l0_d0_e30000_r0.05.dist.gv serran_genome_renamed.fasta.scaff_s98_c4_l0_d0_e30000_r0.05.dist.gv
ln -s /media/bigvol/peguerin/donnees/serran_genome_renamed.fasta serran_genome_renamed.fasta
touch empty.fof

## Running LINKS...
bash LINKS.sh c4r0.05e30000 serran_genome_renamed.fasta

