## download files

## sur MARBEC /media/superdisk/reservebenefit/donnees/genomes
scp serran_genome_lgt3000.fasta  peguerin@162.38.181.163:/media/bigvol/peguerin/donnees/

## sur MBB /share/reservebenefit/RAW/RESERVEBENEFIT/whole_genome_sequencing/wgs_serran/10Xgenomics
scp * peguerin@162.38.181.163:/media/bigvol/peguerin/donnees/

mkdir ~/donnees/lib10x
cd ~/donnees/
mv Lib_*gz lib10x/
